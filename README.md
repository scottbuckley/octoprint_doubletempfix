# Fix for Creality Double Temps

I noticed that my new Ender 3 v2 was sending temperature logs to octoprint in the following format, when it wasn't printing:

    TT::19.7319.73  //0.000.00  BB::19.3719.37  //0.000.00  @@::00  BB@@::00

.. when it should look like the following:

    T:19.73 /0.00 B:19.37 /0.00 @:0 B@:0

Dump the file `DoubleTempFix.py` in your `~/.octoprint/plugins/` directory, and restart OctoPrint, and you should be good.

It works for me, anyway :D
