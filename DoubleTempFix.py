# coding=utf-8

import octoprint.plugin
import re
#import logging

class DoubleTempFixPlugin(octoprint.plugin.OctoPrintPlugin):
    # this is to fix temperature log messages that look like this:
    # "TT::19.7319.73  //0.000.00  BB::19.3719.37  //0.000.00  @@::00  BB@@::00"
    def log(self, comm_instance, line, *args, **kwargs):
        if (re.match("^\s*TT::", line)):
            line = re.sub(r"TT::(\d+\.\d+)\1\s+\/\/(\d+\.\d+)\2", "T:\\1/\\2", line, 1)
            line = re.sub(r"BB::(\d+\.\d+)\1\s+\/\/(\d+\.\d+)\2", "B:\\1/\\2", line, 1)
            line = re.sub(r"@@::(\d+)\1", "@:\\1", line, 1)
            line = re.sub(r"BB@@::(\d+)\1", "B@:\\1", line, 1)
            #logging.getLogger("octoprint.plugin." + __name__).info("DUBBL: "+line)
        return line

__plugin_name__ = "Double Temp Fix"
def __plugin_load__():
    global __plugin_implementation__
    __plugin_implementation__ = DoubleTempFixPlugin()

    global __plugin_hooks__
    __plugin_hooks__ = {
        "octoprint.comm.protocol.gcode.received": __plugin_implementation__.log
    }
